(function(){

  angular.module('UserRegister').run(EventHandler);

  function EventHandler($rootScope, $location){
    $rootScope.$on('$routeChangeError', function(event, next, previous, error){
      if(error='AUTH_REQUIRED') {
        $rootScope.message = 'Sorry, you must log in to access that page';
        $location.path('/login');
      }
    });
  }

})();