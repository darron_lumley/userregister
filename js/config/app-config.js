(function () {
  angular.module('UserRegister').config(['$routeProvider', Router]);

  function Router($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'RegistrationController',
        controllerAs: 'registrationCtrl'
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegistrationController',
        controllerAs: 'registrationCtrl'
      })
      .when('/meetings', {
        templateUrl: 'views/meetings.html',
        controller: 'MeetingsController',
        controllerAs: 'meetingCtrl',
        resolve: {
          currentAuth: function(Authentication){
            return Authentication.requireAuth();
          }
        }
      })
      .when('/checkins/:uId/:mId', {
        templateUrl: 'views/checkins.html',
        controller: 'CheckinsController',
        controllerAs: 'checkinCtrl'
      })
      .when('/checkins/:uId/:mId/checkinsList', {
        templateUrl: 'views/checkinslist.html',
        controller: 'CheckinsController',
        controllerAs: 'checkinCtrl'
      })
      .otherwise({
        redirectTo: '/login'
      });
  }
})();