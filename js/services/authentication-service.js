(function () {

  angular.module('UserRegister').factory('Authentication', Authentication);

  function Authentication($rootScope, $firebaseAuth, $firebaseObject) {
    var me = this;
    var auth = $firebaseAuth();

    auth.$onAuthStateChanged(function(authUser){
      if(authUser){
        var userRef = firebase.database().ref('users/' + authUser.uid);
        var userObj = $firebaseObject(userRef);
        $rootScope.currentUser = userObj;
      } else {
        $rootScope.currentUser = '';
      }
    });

    return {
      login: function (user) {
        return auth.$signInWithEmailAndPassword(user.email, user.password);
      },
      logout: function() {
        return auth.$signOut();
      },
      requireAuth: function() {
        return auth.$requireSignIn();
      },
      register: function (user) {
        return auth.$createUserWithEmailAndPassword(user.email, user.password);
      },
      storeUser: function(user, regUser){
        return firebase.database().ref('users')
          .child(regUser.uid).set({
            date: firebase.database.ServerValue.TIMESTAMP,
            regUser: regUser.uid,
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email
          });
      }
    }

  }

})();