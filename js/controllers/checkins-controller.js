(function () {
  angular.module('UserRegister').controller('CheckinsController', CheckinsController);

  function CheckinsController($rootScope, $location, $firebaseObject, $firebaseArray, $routeParams) {
    var me = this;

    me.message = '';
    me.whichMeeting = $routeParams.mId;
    me.whichUser = $routeParams.uId;
    me.addCheckin = AddCheckin;
    me.deleteCheckin = DeleteCheckin;
    me.pickRandom = PickRandom;
    me.showLove = ShowLove;
    me.giveLove = GiveLove;
    me.deleteLove = DeleteLove;

    me.order = "firstname";
    me.direction = null;
    me.query = '';
    me.recordId = '';

    var checkinsRef = firebase.database().ref('users/' + me.whichUser + '/meetings/' + me.whichMeeting + '/checkins');

    var checkinsList = $firebaseArray(checkinsRef);
    me.checkins = checkinsList;

    function AddCheckin() {
      var meetingData = {
        firstname: me.user.firstname,
        lastname: me.user.lastname,
        email: me.user.email,
        date: firebase.database.ServerValue.TIMESTAMP
      };

      checkinsList.$add(meetingData)
        .then(function (data) {
          $location.path('/checkins/' + me.whichUser + '/' + me.whichMeeting + '/checkinsList');
        }).catch(function (error) {
        console.log(error.message);
      });
    }

    function DeleteCheckin(id) {
      var refDel = firebase.database().ref('users/' + me.whichUser + '/meetings/' + me.whichMeeting + '/checkins/' + id);

      var record = $firebaseObject(refDel);
      record.$remove(id);

    }

    function PickRandom() {
      var whichRecord = Math.round(Math.random() * (checkinsList.length - 1));
      me.recordId = checkinsList.$keyAt(whichRecord);
    }

    function ShowLove(checkin){
      checkin.show = !checkin.show;

      if(checkin.userState == 'expanded'){
        checkin.userState = '';
      } else {
        checkin.userState = 'expanded';
      }
    }

    function GiveLove(checkin, giftText){
      var loveRef = firebase.database().ref('users/' + me.whichUser + '/meetings/' + me.whichMeeting + '/checkins/' + checkin.$id + '/awards');

      var checkinsArray = $firebaseArray(loveRef);

      var loveData = {
        name: giftText,
        date: firebase.database.ServerValue.TIMESTAMP
      };

      checkinsArray.$add(loveData);
    }

    function DeleteLove(checkinId, award){
      var loveRef = firebase.database().ref('users/' + me.whichUser + '/meetings/' + me.whichMeeting + '/checkins/' + checkinId + '/awards/' + award);

      var record = $firebaseObject(loveRef);
      record.$remove(award);
    }

  }

})();

