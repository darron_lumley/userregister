(function () {
  angular.module('UserRegister').controller('RegistrationController', RegistrationController);

  function RegistrationController($location, Authentication) {
    var me = this;

    me.login = Login;
    me.logout = Logout;
    me.register = Register;

    function Login() {
      Authentication.login(me.user)
        .then(function(regUser){
          $location.path('/meetings');
        })
        .catch(function(error){
          me.message = error.message;
        });
    }

    function Logout() {
      Authentication.logout();
    }

    function Register() {
      Authentication.register(me.user)
        .then(function (regUser) {
          Authentication.storeUser(me.user, regUser);
          Login();
        })
        .catch(function (error) {
          me.message = error.message;
        });
    }

  }

})();