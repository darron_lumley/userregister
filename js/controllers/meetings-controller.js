(function () {
  angular.module('UserRegister').controller('MeetingsController', MeetingsController);

  function MeetingsController($rootScope, $firebaseAuth, $firebaseArray) {
    var me = this;
    me.meetingName = '';
    var auth = $firebaseAuth();

    auth.$onAuthStateChanged(function (authUser) {
      if (authUser) {
        var meetingsRef = firebase.database().ref('users/' + authUser.uid + '/meetings');

        var meetingsInfo = $firebaseArray(meetingsRef);

        me.meetings = meetingsInfo;

        meetingsInfo.$loaded().then(function(data){
          $rootScope.howManyMeetings = meetingsInfo.length;
        });

        meetingsInfo.$watch(function(data){
          $rootScope.howManyMeetings = meetingsInfo.length;
        });

        me.addMeeting = function () {
          meetingsInfo.$add(
            {
              name: me.meetingName,
              date: firebase.database.ServerValue.TIMESTAMP
            }
          ).then(function () {
            me.meetingName = '';
          }).catch(function (error) {
            console.log(error.message);
          });
        };

        me.deleteMeeting = function(key){
          meetingsInfo.$remove(key)
        };
      }
    });
  }
})();